FROM openjdk:17-jdk-slim
ARG ENV
ENV DEPLOY_ENV=$ENV
ENV TZ=Asia/Baku
COPY auth-service.jar ./app.jar
ENTRYPOINT ["java", "-jar", "./app.jar", "-Duser.timezone=${TZ}", "--spring.profiles.active=${DEPLOY_ENV}"]
CMD [""]
