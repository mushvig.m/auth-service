package com.banking.authservice.rest;

import com.banking.authservice.dto.ProfileRequest;
import com.banking.authservice.service.ProfileService;
import com.banking.authservice.util.ServiceResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/api/v1/profile", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class ProfileController {
  private final ProfileService profileService;

  @PostMapping(value = "/update")
  public ResponseEntity<ServiceResponse<?>> update(
      @RequestHeader("User-Id") Long merchantId, @RequestBody ProfileRequest request) {

    return ResponseEntity.ok(
        ServiceResponse.success(profileService.updateMerchantProfile(merchantId,request)));
  }

  @GetMapping
  public ResponseEntity<ServiceResponse<?>> getProfile(
      @RequestHeader("User-Id") Long merchantId) {
// marka ve shopname varsa completed true
    return ResponseEntity.ok(
        ServiceResponse.success(profileService.getProfile(merchantId)));
  }

}
