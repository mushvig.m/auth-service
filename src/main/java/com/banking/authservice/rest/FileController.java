package com.banking.authservice.rest;

import com.banking.authservice.util.FileUploadHelper;
import com.banking.authservice.util.ServiceResponse;
import java.io.File;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(value = "/api/v1/file", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class FileController {

  private final FileUploadHelper fileUploadHelper;
  @PostMapping(value = "/upload")
  public ResponseEntity<ServiceResponse<?>> upload(@RequestPart("file") MultipartFile file) {
    String originalFilename = file.getOriginalFilename();
    String newFilename = fileUploadHelper.generateNewFilename(originalFilename);
    File targetFile = new File(newFilename);

    try {
      file.transferTo(targetFile);
    } catch (IOException e) {
      e.printStackTrace();
      //burada error qaytar
//      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
//          .body(ServiceResponse.error(ResponseCode.SYSTEM_ERROR, "Error uploading file"));
    }
    return ResponseEntity.ok(ServiceResponse.success(newFilename));
  }
}
