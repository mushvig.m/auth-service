package com.banking.authservice.rest;

import com.banking.authservice.dao.repository.UserRepository;
import com.banking.authservice.dto.OtpRequestDto;
import com.banking.authservice.dto.PasscodeRequest;
import com.banking.authservice.dto.RefreshRequestDto;
import com.banking.authservice.dto.VerifyOtpRequest;
import com.banking.authservice.service.UserService;
import com.banking.authservice.util.JwtHelper;
import com.banking.authservice.util.ServiceResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping(value = "/api/v1/auth", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class AuthenticationController {
  private final UserService userService;
  private final JwtHelper jwtHelper;
  private final UserRepository userRepository;


  @PostMapping(value = "/otp")
  public ResponseEntity<ServiceResponse<?>> sendOtp(@Valid @RequestBody OtpRequestDto request) {

    return ResponseEntity.ok(
        ServiceResponse.success(userService.sendOtp(request)));
  }

  @PostMapping(value = "/otp/verify")
  public ResponseEntity<ServiceResponse<?>> verifyOtp(@Valid @RequestBody VerifyOtpRequest request) {

    return ResponseEntity.ok(
        ServiceResponse.success(userService.verifyOtp(request)));
  }

//  @PostMapping(value = "/passcode")
//  public ResponseEntity<ServiceResponse<?>> setOrVerifyPasscode(@Valid @RequestBody PasscodeRequest request) {
//
//    return ResponseEntity.ok(
//        ServiceResponse.success(userService.setOrVerifyPasscode(request)));
//  }

  @PostMapping(value = "/passcode/set")
  public ResponseEntity<ServiceResponse<?>> setPasscode(@Valid @RequestBody PasscodeRequest request) {

    return ResponseEntity.ok(
        ServiceResponse.success(userService.setPasscode(request)));
  }

  @PostMapping(value = "/passcode/check")
  public ResponseEntity<ServiceResponse<?>> checkPasscode(
      @Valid @RequestBody PasscodeRequest request) {

    return ResponseEntity.ok(
        ServiceResponse.success(userService.checkPasscode(request)));

  }


  @PostMapping(value = "/refresh")
  public ResponseEntity<ServiceResponse<?>> refreshToken(@Valid @RequestBody RefreshRequestDto request) {
    if (jwtHelper.isTokenExpired(request.getRefreshToken())) {
     //later return exception
    }

    return ResponseEntity.ok(
        ServiceResponse.success(jwtHelper.refreshTokens(request.getRefreshToken())));
  }

}
