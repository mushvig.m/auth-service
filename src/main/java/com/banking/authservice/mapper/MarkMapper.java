package com.banking.authservice.mapper;

import com.banking.authservice.dao.entity.MarksEntity;
import com.banking.authservice.dto.MarksRequest;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class MarkMapper {
  public static MarksRequest mapToRequest(List<MarksEntity> entities) {
    List<Long> markIds = entities.stream()
        .map(MarksEntity::getMarkId)
        .collect(Collectors.toList());

    return MarksRequest.builder().marksList(markIds).build();
  }

}