package com.banking.authservice;

import com.banking.authservice.dto.OtpProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableConfigurationProperties({OtpProperties.class})
@EnableFeignClients
@SpringBootApplication
public class AuthServiceApplication {

  public static void main(String[] args) {
    System.setProperty("server.servlet.context-path", "/auth");

    SpringApplication.run(AuthServiceApplication.class, args);
  }

}
