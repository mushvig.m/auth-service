package com.banking.authservice.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class LocationDto {
  private String longitude;
  private String latitude;
}
