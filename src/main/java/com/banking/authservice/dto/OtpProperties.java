package com.banking.authservice.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "sms")
public record OtpProperties(Api api, Properties properties) {
  public record Api(String baseUrl, String account, String username, String password) {

  }

  public record Properties(Integer maxAttemptCount, Integer blockTime, Integer expirationTime,
                           Integer maxSendOtpCount) {

  }
}
