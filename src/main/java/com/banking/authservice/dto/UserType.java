package com.banking.authservice.dto;

public enum UserType {
  MERCHANT, CUSTOMER
}
