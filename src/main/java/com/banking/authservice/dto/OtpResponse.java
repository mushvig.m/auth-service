package com.banking.authservice.dto;

import com.banking.authservice.util.OtpStatus;
import com.banking.authservice.util.enums.FlowType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtpResponse {
  public Boolean isExistingUser;
  public FlowType flowType;
  public OtpStatus status;
  public Long expirationTime;
}
