package com.banking.authservice.dto;

public record VerifyOtpRequest(String msisdn, String otp) {
}
