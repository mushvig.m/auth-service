package com.banking.authservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OtpRequestDto {
  private String mobileNumber;
  private String deviceId;
  private UserType userType;
}
