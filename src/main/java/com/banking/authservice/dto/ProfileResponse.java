package com.banking.authservice.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProfileResponse {
  private String mobileNumber;
  private String fullName;
  private String shopName;
  private String address;
  private LocationDto location;
  private String img;
  private IsCompleted isCompeted;
  private MarkResponsesDto marks;
}
