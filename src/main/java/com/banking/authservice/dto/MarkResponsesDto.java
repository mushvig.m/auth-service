package com.banking.authservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class MarkResponsesDto {
    private String path;
    private List<MarkResponseDto> list;
}

