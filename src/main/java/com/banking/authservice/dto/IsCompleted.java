package com.banking.authservice.dto;

public enum IsCompleted {
  Y, N
}
