package com.banking.authservice.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MarkResponseDto {
  private Long id;
  private String logo;
  private String name;
}
