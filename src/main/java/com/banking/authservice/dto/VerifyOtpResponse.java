package com.banking.authservice.dto;

import com.banking.authservice.util.OtpStatus;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public record VerifyOtpResponse(OtpStatus status, Long expirationTime) {
}
