package com.banking.authservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasscodeRequest {
  private String mobileNumber;
  private String passcode;
}
