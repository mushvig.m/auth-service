package com.banking.authservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckPasscodeRequest {
  private String mobileNumber;
  private String passcode;
}
