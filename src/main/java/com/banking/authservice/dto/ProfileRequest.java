package com.banking.authservice.dto;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProfileRequest {
  private String fullName;
  private String shopName;
  private String address;
  private LocationDto location;
  private String img;
//  private MarkRequest marks;
  private List<Long> marks;
}
