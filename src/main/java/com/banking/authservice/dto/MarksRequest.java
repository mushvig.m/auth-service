package com.banking.authservice.dto;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class MarksRequest {
  private List<Long> marksList;

}