package com.banking.authservice.config;

import static org.springframework.security.config.Customizer.withDefaults;

import com.banking.authservice.service.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration {
  private final UserDetailsServiceImpl userDetailsService;
//  private final AuthEntryPoint authEntryPoint;
  private static final String[] WHITE_LIST = {
      "/auth/swagger-ui/**",
      "/auth/v3/api-docs/**",
      "/auth/swagger-resources/**"
  };

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    return http
        .csrf(AbstractHttpConfigurer::disable)
        .cors(AbstractHttpConfigurer::disable)

        .authorizeHttpRequests((authz) -> authz

//            .requestMatchers("/auth/api/v1/**").permitAll()
//            .requestMatchers(WHITE_LIST).permitAll()
//            .anyRequest().authenticated()
            //or if you want permit everywhere
                .anyRequest().permitAll()

        ) //.httpBasic(Customizer.withDefaults())
        //.exceptionHandling(Customizer.withDefaults()).build();

        .httpBasic(withDefaults()).build();
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public AuthenticationManager authenticationManager(HttpSecurity http) throws Exception {
    AuthenticationManagerBuilder authenticationManagerBuilder =
        http.getSharedObject(AuthenticationManagerBuilder.class);
    authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    return authenticationManagerBuilder.build();
  }
}
