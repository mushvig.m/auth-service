package com.banking.authservice.exception;

import com.banking.authservice.util.ErrorData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserException extends BaseException {

  public UserException(ErrorTitle errorTitle, ErrorData errorData) {
    super(errorTitle, errorData);
  }

  public UserException(ErrorTitle errorTitle, Object parameter, ErrorData errorData) {
    super(errorTitle, parameter, errorData);
  }

  public static UserException userDoesntHaveWallet(String msisdn, ErrorData data) {
    return new UserException(ErrorTitle.USER_DOESNT_HAVE_WALLET, msisdn, data);
  }

  public static UserException userNotFound(String msisdn) {
    return new UserException(ErrorTitle.USER_NOT_FOUND, msisdn, null);
  }

  public static UserException invalidPasscode(String msisdn) {
    return new UserException(ErrorTitle.USER_PASSCODE_INVALID, msisdn, null);
  }

  public static UserException invalidPin(String msisdn) {
    return new UserException(ErrorTitle.USER_PIN_INVALID, msisdn, null);
  }

  public static UserException userAlreadyExists(String msisdn) {
    return new UserException(ErrorTitle.USER_ALREADY_EXISTS, msisdn, null);
  }

  public static UserException userIsDisabled(String msisdn) {
    return new UserException(ErrorTitle.USER_IS_DISABLED, msisdn, null);
  }

  public static UserException userHasSimaAttr(String msisdn) {
    return new UserException(ErrorTitle.USER_HAS_SIMA_ATTRIBUTE, msisdn, null);
  }

}
