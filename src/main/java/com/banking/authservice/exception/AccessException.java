package com.banking.authservice.exception;

import com.banking.authservice.util.ErrorData;
import lombok.Getter;

@Getter
public class AccessException extends BaseException {

  public AccessException(ErrorTitle errorTitle, ErrorData errorData) {
    super(errorTitle, errorData);
  }

  public AccessException(ErrorTitle errorTitle, Object parameter, ErrorData errorData) {
    super(errorTitle, parameter, errorData);
  }

  public static AccessException apiKeyMismatch() {
    return new AccessException(ErrorTitle.API_KEY_MISMATCH, "API KEY", null);
  }

  public static AccessException otpMaxAttemptLimitReached(String msisdn, Long retryAfterSeconds) {
    return new AccessException(
        ErrorTitle.OTP_MAX_ATTEMPT_LIMIT,
        msisdn,
        ErrorData.builder()
            .expirationTime(retryAfterSeconds)
            .build());
  }

  public static AccessException pinMaxAttemptLimitReached(String msisdn, Long retryAfterSeconds) {
    return new AccessException(
        ErrorTitle.PIN_MAX_ATTEMPT_LIMIT,
        msisdn,
        ErrorData.builder()
            .expirationTime(retryAfterSeconds)
            .build());
  }

  public static AccessException signInMaxAttemptLimitReached(String msisdn, Long retryAfterSeconds) {
    return new AccessException(
        ErrorTitle.SIGN_IN_MAX_ATTEMPT_LIMIT,
        msisdn,
        ErrorData.builder()
            .expirationTime(retryAfterSeconds)
            .build());
  }

  public static AccessException otpFailedException(String msisdn) {
    return new AccessException(ErrorTitle.OTP_FAILED, msisdn, null);
  }

  public static AccessException otpExpiredException(String msisdn) {
    return new AccessException(ErrorTitle.OTP_EXPIRED, msisdn, null);
  }

  public static AccessException otpBlockedException(String msisdn, Long retryAfterSeconds) {
    return new AccessException(
        ErrorTitle.OTP_BLOCKED,
        msisdn,
        ErrorData.builder()
            .expirationTime(retryAfterSeconds)
            .build());
  }

  public static AccessException deviceIdMismatch(String msisdn,String deviceToken) {
    return new AccessException(ErrorTitle.DEVICE_ID_INVALID, msisdn,
            ErrorData.builder()
                    .deviceToken(deviceToken)
                    .build());
  }


}
