package com.banking.authservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorTitle {

  SOMETHING_WENT_WRONG("Something went wrong, please try again."),
  FEIGN_FAILED("Feign client failed: '{}'."),
  VALIDATION_FAILED("Validation failed: '{}'."),
  NOT_AZERCELL_NUMBER("Number '{}' is not Azercell number."),
  CORPORATE_NUMBER("Number '{}' is corporate number."),
  INVALID_NUMBER("Number '{}' is invalid."),
  OTP_MAX_ATTEMPT_LIMIT("Maximum otp attempt limit reached for user with msisdn '{}'."),
  PIN_MAX_ATTEMPT_LIMIT("Maximum pin attempt limit reached for user with msisdn '{}'."),
  SIGN_IN_MAX_ATTEMPT_LIMIT("Maximum sign in attempt limit reached for user with msisdn '{}'."),
  OTP_FAILED("Otp is failed for user with msisdn '{}'."),
  OTP_EXPIRED("Otp is expired for user with msisdn '{}'."),
  OTP_BLOCKED("Otp is blocked for user with msisdn '{}'."),
  USER_NOT_FOUND("User with username '{}' not found."),
  USER_PASSCODE_INVALID("User with username '{}' entered invalid passcode."),
  USER_PIN_INVALID("User with msisdn '{}' entered invalid pin."),
  USER_DOESNT_HAVE_WALLET("User with with msisdn '{}' doesn't have wallet."),
  USER_ALREADY_EXISTS("User with msisdn '{}' is already exists."),
  USER_IS_DISABLED("User with msisdn '{}' has been disabled."),
  USER_HAS_SIMA_ATTRIBUTE("User with msisdn '{}' has SIMA attribute."),
  ACCESS_TOKEN_INVALID("Access token invalid. '{}'"),
  DEVICE_TOKEN_INVALID("Device token invalid. '{}'"),
  DEVICE_TOKEN_OPERATION_FAILED("Device token operation failed during encryption/decryption.'{}'."),
  DEVICE_ID_INVALID("Device id invalid for user with msisdn '{}'."),
  API_KEY_MISMATCH("Mismatched.");

  private final String message;

}
