package com.banking.authservice.exception.handler;



import com.banking.authservice.exception.AccessException;
import com.banking.authservice.exception.NumberException;
import com.banking.authservice.exception.TokenException;
import com.banking.authservice.exception.UserException;
import com.banking.authservice.exception.UtilException;
import com.banking.authservice.util.ServiceResponse;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(AccessException.class)
  public ResponseEntity<ServiceResponse<Void>> handleAccessException(AccessException exception) {
    log.error("Handling access exception: {}", exception.getMessage());
    var status = switch (exception.getErrorTitle()) {
      case OTP_BLOCKED,
          OTP_MAX_ATTEMPT_LIMIT,
          PIN_MAX_ATTEMPT_LIMIT,
          SIGN_IN_MAX_ATTEMPT_LIMIT,
          DEVICE_ID_INVALID,
          API_KEY_MISMATCH -> HttpStatus.FORBIDDEN;

      case OTP_FAILED,
          OTP_EXPIRED -> HttpStatus.CONFLICT;

      default -> HttpStatus.INTERNAL_SERVER_ERROR;
    };
    return new ResponseEntity<>(ServiceResponse.error(exception), status);
  }

  @ExceptionHandler(TokenException.class)
  public ResponseEntity<ServiceResponse<Void>> handleTokenException(TokenException exception) {
    log.error("Handling access exception: {}", exception.getMessage());
    var status = switch (exception.getErrorTitle()) {
      case DEVICE_TOKEN_INVALID,
          DEVICE_TOKEN_OPERATION_FAILED -> HttpStatus.CONFLICT;

      case ACCESS_TOKEN_INVALID -> HttpStatus.UNAUTHORIZED;

      default -> HttpStatus.INTERNAL_SERVER_ERROR;
    };
    return new ResponseEntity<>(ServiceResponse.error(exception), status);
  }

  @ExceptionHandler(NumberException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  public ServiceResponse<Void> handleNumberException(NumberException exception) {
    log.error("Handling number exception: {}", exception.getMessage());
    return ServiceResponse.error(exception);
  }

  @ExceptionHandler(UtilException.class)
  public ResponseEntity<ServiceResponse<Void>> handleUtilException(UtilException exception) {
    log.error("Handling common exception: {}", exception.getMessage());
    var status = switch (exception.getErrorTitle()) {
      case VALIDATION_FAILED -> HttpStatus.CONFLICT;

      case FEIGN_FAILED -> HttpStatus.BAD_GATEWAY;

      default -> HttpStatus.INTERNAL_SERVER_ERROR;
    };
    return new ResponseEntity<>(ServiceResponse.error(exception), status);
  }

  @ExceptionHandler(UserException.class)
  public ResponseEntity<ServiceResponse<Void>> handleUserException(UserException exception) {
    log.error("Handle user exception: {}", exception.getMessage());
    var status = switch (exception.getErrorTitle()) {
      case USER_DOESNT_HAVE_WALLET,
          USER_NOT_FOUND -> HttpStatus.NOT_FOUND;

      case USER_PASSCODE_INVALID,
          USER_PIN_INVALID,
          USER_IS_DISABLED,
          USER_ALREADY_EXISTS,
          USER_HAS_SIMA_ATTRIBUTE -> HttpStatus.CONFLICT;

      default -> HttpStatus.INTERNAL_SERVER_ERROR;
    };
    return new ResponseEntity<>(ServiceResponse.error(exception), status);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                HttpHeaders headers,
                                                                HttpStatusCode status,
                                                                WebRequest request) {
    var message = ex
        .getBindingResult()
        .getAllErrors()
        .stream()
        .map(error -> {
          if (error instanceof FieldError fieldError) {
            return fieldError.getDefaultMessage();
          }
          return error.getDefaultMessage();
        }).collect(Collectors.joining());

    return handleExceptionInternal(
        ex,
        ServiceResponse.error(UtilException.validationFailed(message)),
        new HttpHeaders(),
        HttpStatus.BAD_REQUEST,
        request);
  }

  @Override
  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
                                                                       HttpHeaders headers,
                                                                       HttpStatusCode status,
                                                                       WebRequest request) {
    log.error("{} : {}", ex.getClass().getSimpleName(), ex.getMessage());
    return handleExceptionInternal(
        ex,
        ServiceResponse.error(UtilException.somethingWentWrong()),
        new HttpHeaders(),
        HttpStatus.METHOD_NOT_ALLOWED,
        request);
  }

  @Override
  protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex,
                                                                        HttpHeaders headers,
                                                                        HttpStatusCode status,
                                                                        WebRequest request) {
    log.error("{} : {}", ex.getClass().getSimpleName(), ex.getMessage());
    String message = ex.getMessage();

    if (ex instanceof MissingRequestHeaderException headerException) {
      message = headerException.getMessage();
    }

    return handleExceptionInternal(
        ex,
        ServiceResponse.error(UtilException.validationFailed(message)),
        new HttpHeaders(),
        HttpStatus.BAD_REQUEST,
        request);
  }
}
