package com.banking.authservice.exception;

import com.banking.authservice.util.ErrorData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TokenException extends BaseException {

  public TokenException(ErrorTitle errorTitle, ErrorData errorData) {
    super(errorTitle, errorData);
  }

  public TokenException(ErrorTitle errorTitle, Object parameter, ErrorData errorData) {
    super(errorTitle, parameter, errorData);
  }

  public static TokenException invalidDeviceToken(String message) {
    return new TokenException(ErrorTitle.DEVICE_TOKEN_INVALID, message, null);
  }

  public static TokenException unableEncryptOrDecryptDeviceToken(String message) {
    return new TokenException(ErrorTitle.DEVICE_TOKEN_OPERATION_FAILED, message, null);
  }

  public static TokenException invalidAccessToken(String message) {
    return new TokenException(ErrorTitle.ACCESS_TOKEN_INVALID, message, null);
  }

}
