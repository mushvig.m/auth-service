package com.banking.authservice.exception;

import com.banking.authservice.util.ErrorData;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NumberException extends BaseException {

  public NumberException(ErrorTitle errorTitle, ErrorData errorData) {
    super(errorTitle, errorData);
  }

  public NumberException(ErrorTitle errorTitle, Object parameter, ErrorData errorData) {
    super(errorTitle, parameter, errorData);
  }

  public static NumberException notAzercellNumber(String msisdn) {
    return new NumberException(
            ErrorTitle.NOT_AZERCELL_NUMBER,
            msisdn,
            ErrorData.builder()
                    .build());
  }

  public static NumberException corporateNumber(String msisdn) {
    return new NumberException(
            ErrorTitle.CORPORATE_NUMBER,
            msisdn,
            ErrorData.builder()
                    .build());
  }

  public static NumberException inValidNumber(String msisdn) {
    return new NumberException(
            ErrorTitle.INVALID_NUMBER,
            msisdn,
            ErrorData.builder()
                    .build());
  }

}
