package com.banking.authservice.client;

import com.banking.authservice.dto.MarkResponseDto;
import com.banking.authservice.dto.MarkResponsesDto;
import com.banking.authservice.dto.MarksRequest;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "products", url = "https://api.zapcast.app/products/api/v1/cars/marks/details")
public interface ProductsClient {

  @PostMapping
  MarkResponsesDto getMaksInfo(@RequestBody MarksRequest marks);

}
