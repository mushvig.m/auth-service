package com.banking.authservice.dao.repository;

import com.banking.authservice.dao.entity.User;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
  Optional<User> findByMobileNumber(String mobileNumber);
  Optional<User> findById(Long id);

}