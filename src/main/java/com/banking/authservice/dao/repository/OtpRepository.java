package com.banking.authservice.dao.repository;


import com.banking.authservice.dao.entity.OtpEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OtpRepository extends JpaRepository<OtpEntity, Long> {
  OtpEntity findByMsisdn(String msisdn);
}
