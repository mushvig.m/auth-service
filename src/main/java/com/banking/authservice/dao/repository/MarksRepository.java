package com.banking.authservice.dao.repository;

import com.banking.authservice.dao.entity.MarksEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface MarksRepository extends JpaRepository<MarksEntity, Long> {
  List<MarksEntity> findByMerchantId(Long merchantId);
  @Transactional
  void deleteByMerchantId(Long merchantId);
}
