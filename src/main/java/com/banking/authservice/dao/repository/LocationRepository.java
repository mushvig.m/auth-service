package com.banking.authservice.dao.repository;

import com.banking.authservice.dao.entity.LocationEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<LocationEntity, Long> {
  Optional<LocationEntity> findByMerchantId(Long merchantId);
}
