package com.banking.authservice.dao.entity;

import com.banking.authservice.dto.IsCompleted;
import com.banking.authservice.dto.UserType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "users")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "device_id")
  private Long deviceId;
  @Column(name = "mobile_number")
  private String mobileNumber;
  private String passcode;
  @Column(name = "full_name")
  private String fullName;
  @Column(name = "shop_name")
  private String shopName;
  private String address;
  private String img;
  @Enumerated(EnumType.STRING)
  private UserType userType;
  @Enumerated(EnumType.STRING)
  private IsCompleted isCompleted;
  @CreationTimestamp
  private Instant createdAt;
  @UpdateTimestamp
  private Instant lastUpdatedAt;
}
