package com.banking.authservice.dao.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.type.YesNoConverter;



@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "otp")
public class OtpEntity {
  @SuppressWarnings("checkstyle:Indentation")
  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(
          name = "UUID",
          strategy = "org.hibernate.id.UUIDGenerator"
  )
  @Column(name = "id", updatable = false, nullable = false)
  private String id;

  @Column(name = "msisdn")
  private String msisdn;

  @Column(name = "otp")
  private String otp;

  @Column(name = "send_otp_count", columnDefinition = "Integer default 1")
  private Integer sendOtpCount;

  @Column(name = "attempt_count", columnDefinition = "Integer default 0")
  private Integer attemptCount;

  @Column(name = "expiration_time")
  private Instant expirationTime;

  @Column(name = "block_time")
  private Instant blockTime;

  @Convert(converter = YesNoConverter.class)
  @Column(name = "is_deleted")
  private Boolean isDeleted;

  @CreationTimestamp
  private Instant createdAt;

  @UpdateTimestamp
  private Instant lastUpdatedAt;
}

