package com.banking.authservice.service;

import com.banking.authservice.client.ProductsClient;
import com.banking.authservice.dao.entity.LocationEntity;
import com.banking.authservice.dao.entity.MarksEntity;
import com.banking.authservice.dao.repository.LocationRepository;
import com.banking.authservice.dao.repository.MarksRepository;
import com.banking.authservice.dao.repository.UserRepository;
import com.banking.authservice.dto.IsCompleted;
import com.banking.authservice.dto.LocationDto;
import com.banking.authservice.dto.MarkResponseDto;
import com.banking.authservice.dto.MarksRequest;
import com.banking.authservice.dto.ProfileRequest;
import com.banking.authservice.dto.ProfileResponse;
import com.banking.authservice.mapper.MarkMapper;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileService {
  private final UserRepository userRepository;
  private final MarksRepository marksRepository;
  private final LocationRepository locationRepository;
  private final ProductsClient productsClient;

  public ProfileResponse updateMerchantProfile(Long merchantId, ProfileRequest request) {
    var isCompleted =
        (request.getMarks().size() > 0 && !request.getShopName().isEmpty()) ? IsCompleted.Y : IsCompleted.N;

    var merchant = userRepository.findById(merchantId).orElseThrow();
    merchant.setAddress(request.getAddress());
    merchant.setImg(request.getImg());
    merchant.setIsCompleted(isCompleted);
    merchant.setShopName(request.getShopName());
    merchant.setFullName(request.getFullName());
    userRepository.save(merchant);

    updateLocation(merchantId, request);

    updateMarks(merchantId, request.getMarks());
    return getProfile(merchantId);
  }

  public void updateLocation(Long merchantId, ProfileRequest request) {
    var location = locationRepository.findByMerchantId(merchantId)
        .orElse(LocationEntity.builder().merchantId(merchantId).build());

    location.setMerchantId(merchantId);
    location.setLatitude(request.getLocation().getLatitude());
    location.setLongitude(request.getLocation().getLongitude());
    locationRepository.save(location);
  }

  public void updateMarks(Long merchantId, List<Long> marks) {
    List<MarksEntity> entities = marks.stream()
        .map(markId -> {
          return MarksEntity.builder().merchantId(merchantId).markId(markId).build();
        })
        .collect(Collectors.toList());
    marksRepository.deleteByMerchantId(merchantId);

    marksRepository.saveAll(entities);
  }

  public ProfileResponse getProfile(Long merchantId) {
    var merchant = userRepository.findById(merchantId).orElseThrow();
    var location = locationRepository.findByMerchantId(merchantId).orElseThrow();
    var marks = productsClient.getMaksInfo(MarkMapper.mapToRequest(marksRepository.findByMerchantId(merchantId)));
    return ProfileResponse.builder()
        .isCompeted(merchant.getIsCompleted())
        .fullName(merchant.getFullName())
        .address(merchant.getAddress())
        .shopName(merchant.getShopName())
        .mobileNumber(merchant.getMobileNumber())
        .img(merchant.getImg()) //todo full path
        .location(LocationDto.builder().latitude(location.getLatitude()).longitude(location.getLongitude()).build())
        .marks(marks)
        .build();
  }
}
