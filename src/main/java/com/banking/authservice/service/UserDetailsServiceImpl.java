package com.banking.authservice.service;

import com.banking.authservice.dao.entity.User;
import com.banking.authservice.dao.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

  private final UserRepository repository;

  @Override
  public UserDetails loadUserByUsername(String mobileNumber) {

    User user = repository.findByMobileNumber(mobileNumber).orElseThrow(() ->
      new UsernameNotFoundException(mobileNumber));

    return org.springframework.security.core.userdetails.User.builder()
        .username(user.getMobileNumber())
        .password(user.getPasscode())
        .build();
  }
}
