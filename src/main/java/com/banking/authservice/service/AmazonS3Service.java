package com.banking.authservice.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.banking.authservice.util.FileUploadHelper;
import java.util.Objects;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class AmazonS3Service {

  private final String bucketName = "your-bucket-name";
  private final String fileUrl = "/uploads/company/logo/";
  private final AmazonS3 s3Client;
  private final FileUploadHelper fileUploadHelper;

  public AmazonS3Service(FileUploadHelper fileUploadHelper) {
    this.fileUploadHelper = fileUploadHelper;

    this.s3Client = AmazonS3ClientBuilder.defaultClient();
  }

  public String uploadFile(MultipartFile file) throws IOException {
    // Dosyayı S3'e yükle
    File convertedFile = convertMultiPartFileToFile(file);
    String key = fileUploadHelper.generateNewFilename(file.getOriginalFilename());
    String fullPath = fileUrl + key;
    PutObjectRequest request = new PutObjectRequest(bucketName, key, convertedFile);
    PutObjectResult result = s3Client.putObject(request);

    // Dosya yükleme işlemi başarılı ise S3'de saklanan dosyanın URL'sini döndür
    return s3Client.getUrl(bucketName, key).toString();
  }

  private File convertMultiPartFileToFile(MultipartFile file) throws IOException {
    File convertedFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
    file.transferTo(convertedFile);
    return convertedFile;
  }


}
