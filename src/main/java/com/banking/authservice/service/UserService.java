package com.banking.authservice.service;

import com.banking.authservice.dao.entity.User;
import com.banking.authservice.dao.repository.UserRepository;
import com.banking.authservice.dto.IsCompleted;
import com.banking.authservice.dto.LoginResponseDto;
import com.banking.authservice.dto.OtpRequestDto;
import com.banking.authservice.dto.OtpResponse;
import com.banking.authservice.dto.PasscodeRequest;
import com.banking.authservice.dto.VerifyOtpRequest;
import com.banking.authservice.exception.UserException;
import com.banking.authservice.util.JwtHelper;
import com.banking.authservice.util.enums.FlowType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {
  private final UserRepository userRepository;
  private final BCryptPasswordEncoder passwordEncoder;
  private final JwtHelper jwtHelper;
  private final AuthenticationManager authenticationManager;
  private final OtpService otpService;


  public OtpResponse sendOtp(OtpRequestDto request) {
    var mobileNumber = request.getMobileNumber();
    var user = userRepository.findByMobileNumber(mobileNumber);

    boolean isExistingUser = user.isPresent() && user.get().getPasscode() != null;

    if (user.isEmpty()) {
      userRepository.save(
          User.builder().userType(request.getUserType()).isCompleted(IsCompleted.N).mobileNumber(mobileNumber).build());
    }

    OtpResponse response = otpService.sendOtp(mobileNumber);
    response.setIsExistingUser(isExistingUser);
    response.setFlowType(isExistingUser ? FlowType.LOGIN : FlowType.REGISTER);

    return response;
  }

  public OtpResponse verifyOtp(VerifyOtpRequest request) {
    return otpService.verifyOtp(request);
  }

  public LoginResponseDto setOrVerifyPasscode(PasscodeRequest request) {
    var user = userRepository.findByMobileNumber(request.getMobileNumber()).orElseThrow();
    if (user.getPasscode() != null) {
      authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(request.getMobileNumber(), request.getPasscode()));
    } else {
      String hashedPassword = passwordEncoder.encode(request.getPasscode());
      user.setPasscode(hashedPassword);
      userRepository.save(user);
    }
    return loginResponseDtoBuilder(user.getId(), request.getMobileNumber());
  }

  public LoginResponseDto checkPasscode(PasscodeRequest request) {
//    authenticationManager.authenticate(
//        new UsernamePasswordAuthenticationToken(request.getMobileNumber(), request.getPasscode()));
    var user = userRepository.findByMobileNumber(request.getMobileNumber())
        .orElseThrow(() -> UserException.userNotFound(request.getMobileNumber()));
    if (!passwordEncoder.matches(request.getPasscode(), user.getPasscode())) {
      throw UserException.invalidPin(request.getMobileNumber());
    }
    return loginResponseDtoBuilder(userRepository.findByMobileNumber(request.getMobileNumber()).get().getId(),
        request.getMobileNumber());

  }

  public LoginResponseDto setPasscode(PasscodeRequest request) {

    String hashedPassword = passwordEncoder.encode(request.getPasscode());
    var user = userRepository.findByMobileNumber(request.getMobileNumber()).orElseThrow();
    user.setPasscode(hashedPassword);
    userRepository.save(user);
    return loginResponseDtoBuilder(user.getId(), request.getMobileNumber());

  }

  public LoginResponseDto loginResponseDtoBuilder(Long merchantId, String mobileNumber) {

    String accessToken = jwtHelper.generateAccessToken( mobileNumber, merchantId);
    String refreshToken = jwtHelper.generateRefreshToken( mobileNumber, merchantId);
    return LoginResponseDto.builder().accessToken(accessToken).refreshToken(refreshToken).build();
  }

}
