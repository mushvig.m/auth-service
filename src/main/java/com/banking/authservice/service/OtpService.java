package com.banking.authservice.service;


import com.banking.authservice.dao.entity.OtpEntity;
import com.banking.authservice.dao.repository.OtpRepository;
import com.banking.authservice.dto.OtpProperties;
import com.banking.authservice.dto.OtpResponse;
import com.banking.authservice.dto.VerifyOtpRequest;
import com.banking.authservice.exception.AccessException;
import com.banking.authservice.util.OtpStatus;
import com.banking.authservice.util.SendSmsHelper;
import java.time.Instant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OtpService {

  private final OtpProperties otpProperties;
  private final OtpRepository otpRepository;
  private final SendSmsHelper sendSmsHelper;

  public OtpResponse sendOtp(String msisdn) {
    var existingOtp = otpRepository.findByMsisdn(msisdn);
    if (existingOtp == null) {
      return handleNewOtp(msisdn);
    } else if (existingOtp.getIsDeleted()) {
      var sentOtp = sendOtpAndUpdate(existingOtp, msisdn);
      return OtpResponse.builder().status(OtpStatus.SUCCESS)
          .expirationTime(expirationTimeResponse(toEpoch(sentOtp.getExpirationTime())))
          .build();
    } else {
      return handleExistingOtp(existingOtp, msisdn, Instant.now());
    }
  }

  public OtpResponse verifyOtp(VerifyOtpRequest otpRequest) {
    var existingOtp = otpRepository.findByMsisdn(otpRequest.msisdn());
    if (existingOtp == null) {
      throw AccessException.otpFailedException(otpRequest.msisdn());

    }

    if (existingOtp.getBlockTime() != null && existingOtp.getBlockTime().isAfter(Instant.now())) {

      throw AccessException.otpBlockedException(otpRequest.msisdn(),
          expirationTimeResponse(toEpoch(existingOtp.getBlockTime())));
    }

    existingOtp.setAttemptCount(existingOtp.getAttemptCount() + 1);

    if (existingOtp.getExpirationTime().isBefore(Instant.now())) {
      otpRepository.save(existingOtp);

      throw AccessException.otpExpiredException(otpRequest.otp());

    }

    if (existingOtp.getOtp().equals(otpRequest.otp())) {
      existingOtp.setIsDeleted(true);
      otpRepository.save(existingOtp);
      return OtpResponse.builder().status(OtpStatus.SUCCESS)
          .build();
    }

    if (existingOtp.getAttemptCount() >= otpProperties.properties().maxAttemptCount()) {
      existingOtp.setBlockTime(Instant.now().plusSeconds(otpProperties.properties().blockTime()));
      otpRepository.save(existingOtp);

      throw AccessException.otpBlockedException(otpRequest.msisdn(),
          expirationTimeResponse(expirationTimeResponse(toEpoch(existingOtp.getBlockTime()))));
    }

    otpRepository.save(existingOtp);
    throw AccessException.otpFailedException(otpRequest.otp());

  }

  private OtpResponse handleExistingOtp(OtpEntity existingOtp, String msisdn, Instant now) {
    if (existingOtp.getSendOtpCount() >= otpProperties.properties().maxSendOtpCount()
        || existingOtp.getAttemptCount() >= otpProperties.properties().maxAttemptCount()) {
      return handleMaxCounts(existingOtp, msisdn, now);
    } else {
      var otp = sendSmsHelper.create(msisdn);
      existingOtp.setOtp(otp);
      existingOtp.setSendOtpCount(existingOtp.getSendOtpCount() + 1);
      existingOtp.setExpirationTime(now.plusSeconds(otpProperties.properties().expirationTime()));
      otpRepository.save(existingOtp);
      sendSmsHelper.sendSms(msisdn, otp);
      return OtpResponse.builder().status(OtpStatus.SUCCESS)
          .expirationTime(expirationTimeResponse(toEpoch(existingOtp.getExpirationTime())))
          .build();
    }
  }

  private OtpResponse handleMaxCounts(OtpEntity existingOtp, String msisdn, Instant now) {
    if (existingOtp.getBlockTime() != null && existingOtp.getBlockTime().isAfter(now)) {
      throw AccessException.otpBlockedException(msisdn, expirationTimeResponse(toEpoch(existingOtp.getBlockTime())));
    } else {
      return handleBlockTime(existingOtp, msisdn, now);
    }
  }

  private OtpResponse handleBlockTime(OtpEntity existingOtp, String msisdn, Instant now) {
    if (existingOtp.getBlockTime() == null) {
      existingOtp.setBlockTime(now.plusSeconds(otpProperties.properties().blockTime()));
      otpRepository.save(existingOtp);
      throw AccessException.otpBlockedException(msisdn, expirationTimeResponse(toEpoch(existingOtp.getBlockTime())));

    } else {
      var updatedOtpEntity = sendOtpAndUpdate(existingOtp, msisdn);
      return OtpResponse.builder().status(OtpStatus.SUCCESS)
          .expirationTime(expirationTimeResponse(toEpoch(updatedOtpEntity.getExpirationTime())))
          .build();
    }
  }

  private OtpResponse handleNewOtp(String msisdn) {
    var otpEntity = createOtp(msisdn);
    sendSmsHelper.sendSms(msisdn, otpEntity.getOtp());
    return OtpResponse.builder().status(OtpStatus.SUCCESS)
        .expirationTime(expirationTimeResponse(toEpoch(otpEntity.getExpirationTime())))
        .build();
  }


  private OtpEntity createOtp(String msisdn) {

    var otp = sendSmsHelper.create(msisdn);
    return otpRepository.save(OtpEntity.builder().msisdn(msisdn).otp(otp)
        .expirationTime(Instant.now().plusSeconds(otpProperties.properties().expirationTime())).attemptCount(0)
        .sendOtpCount(1).isDeleted(false).build());

  }


  private OtpEntity updateOtp(OtpEntity existingOtp) {
    var otp = sendSmsHelper.create(existingOtp.getMsisdn());
    existingOtp.setOtp(otp);
    existingOtp.setExpirationTime(Instant.now().plusSeconds(otpProperties.properties().expirationTime()));
    existingOtp.setAttemptCount(0);
    existingOtp.setSendOtpCount(1);
    existingOtp.setBlockTime(null);
    existingOtp.setIsDeleted(false);
    return otpRepository.save(existingOtp);
  }

  private OtpEntity sendOtpAndUpdate(OtpEntity existingOtp, String msisdn) {
    var otpEntity = updateOtp(existingOtp);
    sendSmsHelper.sendSms(msisdn, otpEntity.getOtp());
    return otpEntity;
  }


  private static Long toEpoch(Instant dateTime) {
    try {
      if (dateTime != null) {
        return dateTime.getEpochSecond();
      }
    } catch (ArithmeticException e) {
      log.error("Error converting to epoch", e);
    }
    return null;
  }

  private Long expirationTimeResponse(Long expirationTime) {
    return expirationTime - toEpoch(Instant.now());
  }

}
