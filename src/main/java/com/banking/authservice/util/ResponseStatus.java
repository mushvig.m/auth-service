package com.banking.authservice.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseStatus {

  OK(0, "processed successfully"),
  FAIL(1, "processing failed");

  private final int code;

  private final String message;
}
