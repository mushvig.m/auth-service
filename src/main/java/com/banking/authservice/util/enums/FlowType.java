package com.banking.authservice.util.enums;

public enum FlowType {
  LOGIN, REGISTER, CHANGE_PASSWORD
}
