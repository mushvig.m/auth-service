package com.banking.authservice.util;

import com.banking.authservice.dto.LoginResponseDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class JwtHelper {

  @Value("${app.jwt.secret}")
  private String SECRET_KEY;

  @Value("${app.jwt.expire.access}")
  private int EXPIRE_ACCESS;
  @Value("${app.jwt.expire.refresh}")
  private int EXPIRE_REFRESH;

  public String generateAccessToken(String username, Long companyId) {

    return Jwts.builder()
        .claim("username", username)
        .claim("companyId", companyId)
        .setExpiration(new Date(System.currentTimeMillis() + EXPIRE_ACCESS))
        .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();

  }

  public String generateRefreshToken(String username, Long companyId) {

    return Jwts.builder()
        .claim("username", username)
        .claim("companyId", companyId)
        .setExpiration(new Date(System.currentTimeMillis() + EXPIRE_REFRESH))
        .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();

  }

  public LoginResponseDto refreshTokens(String token) {

    Jws<Claims> claims = Jwts.parser()
        .setSigningKey(SECRET_KEY)
        .parseClaimsJws(token);

    String username = claims.getBody().get("username", String.class);
    Long companyId = claims.getBody().get("companyId", Long.class);

    var refreshToken = Jwts.builder()
        .claim("username", username)
        .claim("companyId", companyId)
        .setExpiration(new Date(System.currentTimeMillis() + EXPIRE_REFRESH))
        .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    var accessToken = generateAccessToken(username, companyId);
    return LoginResponseDto.builder().accessToken(accessToken).refreshToken(refreshToken).build();
  }

//  public String extractUsername(String token) {
//    return getTokenBody(token).getSubject();
//  }

//  public Boolean validateToken(String token, UserDetails userDetails) {
//    final String username = extractUsername(token);
//    return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
//  }

  public Claims getTokenBody(String token) {
    try {
      return Jwts.parser()
          .setSigningKey(SECRET_KEY)
          .parseClaimsJws(token)
          .getBody();
    } catch (ExpiredJwtException e) { // Invalid signature or expired token
      throw new AccessDeniedException("Access denied: " + e.getMessage());
    }
  }

  public boolean isTokenExpired(String token) {
    Claims claims = getTokenBody(token);
    return claims.getExpiration().before(new Date());
  }
}