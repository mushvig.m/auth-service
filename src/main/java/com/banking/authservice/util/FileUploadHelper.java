package com.banking.authservice.util;

import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class FileUploadHelper {
  public String generateNewFilename(String fileName) {
    return UUID.randomUUID().toString() + '_' + System.currentTimeMillis() + "." + getFileExtension(fileName);
  }

  private String getFileExtension(String filename) {
    int lastIndex = filename.lastIndexOf('.');
    if (lastIndex == -1) {
      return ""; // If there is no extension, return an empty string
    }
    return filename.substring(lastIndex + 1);
  }
}
