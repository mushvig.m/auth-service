package com.banking.authservice.util;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import com.banking.authservice.exception.ErrorTitle;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@JsonInclude(NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {

  private ErrorTitle errorCode;
  private ErrorTitle title;

  private String message;
  private ErrorData data;

}
