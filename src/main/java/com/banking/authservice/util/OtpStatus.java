package com.banking.authservice.util;

public enum OtpStatus {

  SUCCESS,
  FAIL,
  EXPIRED,
  BLOCKED
}
